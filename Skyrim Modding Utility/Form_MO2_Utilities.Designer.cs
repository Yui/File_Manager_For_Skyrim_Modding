﻿namespace Skyrim_Modding_Utility
{
    partial class Form_MO2_Utilities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rBHide = new System.Windows.Forms.RadioButton();
            this.rBDelete = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.BTOK = new System.Windows.Forms.Button();
            this.rBSeparators = new System.Windows.Forms.RadioButton();
            this.labelDirInfo = new System.Windows.Forms.Label();
            this.CBIgnoreWS = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // rBHide
            // 
            this.rBHide.AutoSize = true;
            this.rBHide.Location = new System.Drawing.Point(13, 13);
            this.rBHide.Name = "rBHide";
            this.rBHide.Size = new System.Drawing.Size(47, 17);
            this.rBHide.TabIndex = 0;
            this.rBHide.TabStop = true;
            this.rBHide.Text = "Hide";
            this.rBHide.UseVisualStyleBackColor = true;
            // 
            // rBDelete
            // 
            this.rBDelete.AutoSize = true;
            this.rBDelete.Location = new System.Drawing.Point(80, 12);
            this.rBDelete.Name = "rBDelete";
            this.rBDelete.Size = new System.Drawing.Size(56, 17);
            this.rBDelete.TabIndex = 0;
            this.rBDelete.TabStop = true;
            this.rBDelete.Text = "Delete";
            this.rBDelete.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 47);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(497, 391);
            this.textBox1.TabIndex = 1;
            // 
            // BTOK
            // 
            this.BTOK.Location = new System.Drawing.Point(538, 47);
            this.BTOK.Name = "BTOK";
            this.BTOK.Size = new System.Drawing.Size(75, 23);
            this.BTOK.TabIndex = 2;
            this.BTOK.Text = "OK";
            this.BTOK.UseVisualStyleBackColor = true;
            this.BTOK.Click += new System.EventHandler(this.BTOK_Click);
            // 
            // rBSeparators
            // 
            this.rBSeparators.AutoSize = true;
            this.rBSeparators.Location = new System.Drawing.Point(152, 12);
            this.rBSeparators.Name = "rBSeparators";
            this.rBSeparators.Size = new System.Drawing.Size(110, 17);
            this.rBSeparators.TabIndex = 0;
            this.rBSeparators.TabStop = true;
            this.rBSeparators.Text = "Create Separators";
            this.rBSeparators.UseVisualStyleBackColor = true;
            // 
            // labelDirInfo
            // 
            this.labelDirInfo.AllowDrop = true;
            this.labelDirInfo.AutoSize = true;
            this.labelDirInfo.Location = new System.Drawing.Point(287, 12);
            this.labelDirInfo.Name = "labelDirInfo";
            this.labelDirInfo.Size = new System.Drawing.Size(35, 13);
            this.labelDirInfo.TabIndex = 3;
            this.labelDirInfo.Text = "label1";
            this.labelDirInfo.DragDrop += new System.Windows.Forms.DragEventHandler(this.labelDirInfo_DragDrop);
            this.labelDirInfo.DragEnter += new System.Windows.Forms.DragEventHandler(this.labelDirInfo_DragEnter);
            // 
            // CBIgnoreWS
            // 
            this.CBIgnoreWS.AutoSize = true;
            this.CBIgnoreWS.Location = new System.Drawing.Point(538, 95);
            this.CBIgnoreWS.Name = "CBIgnoreWS";
            this.CBIgnoreWS.Size = new System.Drawing.Size(118, 17);
            this.CBIgnoreWS.TabIndex = 4;
            this.CBIgnoreWS.Text = "Ignore WhiteSpace";
            this.CBIgnoreWS.UseVisualStyleBackColor = true;
            // 
            // Form_MO2_Utilities
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.CBIgnoreWS);
            this.Controls.Add(this.labelDirInfo);
            this.Controls.Add(this.BTOK);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.rBSeparators);
            this.Controls.Add(this.rBDelete);
            this.Controls.Add(this.rBHide);
            this.Name = "Form_MO2_Utilities";
            this.Text = "MO2 Utilities";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_MO2_Utilities_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rBHide;
        private System.Windows.Forms.RadioButton rBDelete;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button BTOK;
        private System.Windows.Forms.RadioButton rBSeparators;
        private System.Windows.Forms.Label labelDirInfo;
        private System.Windows.Forms.CheckBox CBIgnoreWS;
    }
}