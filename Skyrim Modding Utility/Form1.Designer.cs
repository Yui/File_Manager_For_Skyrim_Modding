﻿namespace Skyrim_Modding_Utility
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTFileHiderMO2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.MO2_Page = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.Other_Page = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.MO2_Page.SuspendLayout();
            this.SuspendLayout();
            // 
            // BTFileHiderMO2
            // 
            this.BTFileHiderMO2.Location = new System.Drawing.Point(42, 109);
            this.BTFileHiderMO2.Name = "BTFileHiderMO2";
            this.BTFileHiderMO2.Size = new System.Drawing.Size(75, 23);
            this.BTFileHiderMO2.TabIndex = 0;
            this.BTFileHiderMO2.Text = "Launch";
            this.BTFileHiderMO2.UseVisualStyleBackColor = true;
            this.BTFileHiderMO2.Click += new System.EventHandler(this.BTFileHiderMO2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(273, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mod organizer 2";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.MO2_Page);
            this.tabControl1.Controls.Add(this.Other_Page);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(776, 426);
            this.tabControl1.TabIndex = 2;
            // 
            // MO2_Page
            // 
            this.MO2_Page.Controls.Add(this.label2);
            this.MO2_Page.Controls.Add(this.BTFileHiderMO2);
            this.MO2_Page.Controls.Add(this.label1);
            this.MO2_Page.Location = new System.Drawing.Point(4, 22);
            this.MO2_Page.Name = "MO2_Page";
            this.MO2_Page.Padding = new System.Windows.Forms.Padding(3);
            this.MO2_Page.Size = new System.Drawing.Size(768, 400);
            this.MO2_Page.TabIndex = 0;
            this.MO2_Page.Text = "MO2";
            this.MO2_Page.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Hide files from MO2";
            // 
            // Other_Page
            // 
            this.Other_Page.Location = new System.Drawing.Point(4, 22);
            this.Other_Page.Name = "Other_Page";
            this.Other_Page.Padding = new System.Windows.Forms.Padding(3);
            this.Other_Page.Size = new System.Drawing.Size(768, 400);
            this.Other_Page.TabIndex = 1;
            this.Other_Page.Text = "Other";
            this.Other_Page.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 457);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Skyrim Modding Utility";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.MO2_Page.ResumeLayout(false);
            this.MO2_Page.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BTFileHiderMO2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage MO2_Page;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage Other_Page;
    }
}

