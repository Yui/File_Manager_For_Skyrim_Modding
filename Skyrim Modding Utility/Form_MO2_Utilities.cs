﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Skyrim_Modding_Utility
{
    public partial class Form_MO2_Utilities : Form
    {
        public Form_MO2_Utilities()
        {
            InitializeComponent();
            labelDirInfo.Text = Directory.GetCurrentDirectory().ToString();
        }

        private void BTOK_Click(object sender, EventArgs e)
        {
            string item;
            int ErrCount = 0;
            if (rBHide.Checked)
            {
                #region File Hider
                
                foreach (string line in textBox1.Lines)
                {
                    if (CBIgnoreWS.Checked)
                    {
                        item = line.Trim();
                    }
                    else
                    {
                        item = line;
                    }

                    try
                    {
                        if (File.Exists(item))
                        {
                            File.Move(item, item + ".mohidden");
                        }
                        else if (Directory.Exists(item))
                        {
                            Directory.Move(item, item + ".mohidden");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(item + Environment.NewLine + ex.Message, "Error");
                        ErrCount++;
                    }
                }
                #endregion File Hider
            }
            else if (rBDelete.Checked)
            {
                #region File Deleter
                foreach (string line in textBox1.Lines)
                {
                    if (CBIgnoreWS.Checked)
                    {
                        item = line.Trim();
                    }
                    else
                    {
                        item = line;
                    }
                    try
                    {
                        if (File.Exists(item))
                        {
                            File.Delete(item);
                        }
                        else if (Directory.Exists(item))
                        {
                            Directory.Delete(item, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(item + Environment.NewLine + ex.Message, "Error");
                        ErrCount++;
                    }
                }
                #endregion File Deleter
            }
            else if (rBSeparators.Checked)
            {
                #region Separators
                if (!Directory.Exists("template"))
                {
                    try
                    {
                        Directory.CreateDirectory("template");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                        ErrCount++;
                    }

                    MessageBox.Show("Create meta.ini file in template folder, than click continue", "Err missing files.");
                }
                else if (!File.Exists("template//meta.ini"))
                {
                    MessageBox.Show("Create meta.ini file in template folder, than click continue", "Err missing files.");
                }
                else
                {
                    string dirname = "";
                    foreach (string line in textBox1.Lines)
                    {
                        dirname = line + "_separator";
                        try
                        {
                            Directory.CreateDirectory(dirname);
                            File.Copy("template//meta.ini", dirname + "//meta.ini");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(line + Environment.NewLine + ex.Message, "Error");
                            ErrCount++;
                        }
                    }
                }
                #endregion Separators
            }
            MessageBox.Show("Task completed with " + ErrCount.ToString() + " errors.", "Warning");
        }

        #region Drag n Drop
        private void labelDirInfo_DragDrop(object sender, DragEventArgs e)
        {
            string[] fileList = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            string path = "";
            foreach (string s in fileList)
            {
                path += s + "\\";
            }
            if (Directory.Exists(path))
            {
                Directory.SetCurrentDirectory(path);
                labelDirInfo.Text = path;
            }
        }

        private void labelDirInfo_DragEnter(object sender, DragEventArgs e)
        {
            //Checks if data comes from Explorer
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        #endregion Drag n Drop

        private void Form_MO2_Utilities_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form1 temp = new Form1();
            temp.Show();            
        }
    }
}
